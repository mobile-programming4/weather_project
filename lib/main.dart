import 'package:flutter/material.dart';

void main() {
  runApp(WeatherApp());
}

class WeatherApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
      ),
    );
  }
}

AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.transparent,
    elevation: 0,
    title: Text(
      "Melbourne",
      style: TextStyle(fontWeight: FontWeight.w300, fontSize: 30),
    ),
    centerTitle: true,
  );
}

Widget buildBodyWidget() {
  return Stack(
    children: <Widget>[
      Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage("https://wallpapercave.com/wp/wp9221668.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: ListView(
          children: <Widget>[
            buildCurrentTemp(),
            buildAlert(),
            buildHorlyForecast(),
            build10DayForecast(),
          ],
        ),
      ),
    ],
  );
}

buildCurrentTemp(){
  return Container(
    height: 200,
    child: ListTile(
      title: Text(
        "13°",
        style: TextStyle(
            fontSize: 80,
            color: Colors.white,
            fontWeight: FontWeight.w200),
        textAlign: TextAlign.center,
      ),
      subtitle: Text(
        "Clear\nH:30°  L:12°",
        style: TextStyle(
            fontSize: 20,
            color: Colors.white,
            fontWeight: FontWeight.w500),
        textAlign: TextAlign.center,
      ),
    ),
  );
}

buildAlert() {
  return Container(
      margin: EdgeInsets.all(10),
  decoration: ShapeDecoration(
  color: Colors.black26,
  shape: RoundedRectangleBorder(
  borderRadius: BorderRadius.circular(20),
  ),
  ),
  height: 140,
  child: ListView(
    children: <Widget>[
      Container(
          margin: EdgeInsets.all(20),
          child: Column(
            children: [
              Row(children: [
                Icon(
                  Icons.announcement,
                  color: Colors.white,
                ),
                Text(
                  "   Extreme Heat",
                  style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
              ]),
              Text(""),
              Text(
                "Australian Government Bureau of Meteorology: Extreme Heat in Central.",
                style: TextStyle(color: Colors.white),
              ),
              Divider(
                color: Colors.grey,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "See More",
                    style: TextStyle(color: Colors.white),
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.grey,
                    size: 18,
                  )
                ],
              )
            ],
          )),
    ],
  ),
  );
}

buildHorlyForecast() {
  return Container(
      margin: EdgeInsets.all(10),
      decoration: ShapeDecoration(
        color: Colors.black26,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
      ),
      height: 155,
      child: Padding(
        padding: EdgeInsets.all(15),
        child: ListView(
          children: [
            Row(
              children: [
                Icon(
                  Icons.access_time,
                  color: Colors.grey,
                ),
                Text("  HOURLY FORECAST",
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.grey,
                    )),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildTemperature01(),
                buildTemperature02(),
                buildTemperature03(),
                buildTemperature04(),
                buildTemperature05(),
                buildTemperature06(),
              ],
            )
          ],
        ),
      ));
}

build10DayForecast() {
  return Container(
    margin: EdgeInsets.all(10),
    decoration: ShapeDecoration(
      color: Colors.black26,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
    ),
    height: 460,
    child: Padding(
      padding: EdgeInsets.all(15),
      child: ListView(
        children: [
          Row(children: [
            Icon(
              Icons.calendar_month,
              color: Colors.grey,
            ),
            Text(
              "  10-DAY FORECAST",
              style: TextStyle(
                color: Colors.grey,
                fontSize: 15,
              ),
            )
          ]),
          Divider(
            color: Colors.grey,
          ),
          Column(
            children: [
              buildTempDay0(),
              Divider(
                color: Colors.grey,
              ),
              buildTempDay1(),
              Divider(
                color: Colors.grey,
              ),
              buildTempDay2(),
              Divider(
                color: Colors.grey,
              ),
              buildTempDay3(),
              Divider(
                color: Colors.grey,
              ),
              buildTempDay4(),
              Divider(
                color: Colors.grey,
              ),
              buildTempDay5(),
              Divider(
                color: Colors.grey,
              ),
              buildTempDay6(),
              Divider(
                color: Colors.grey,
              ),
              buildTempDay7(),
              Divider(
                color: Colors.grey,
              ),
              buildTempDay1(),
              Divider(
                color: Colors.grey,
              ),
              buildTempDay2(),
            ],
          )
        ],
      ),
    ),
  );
}

buildTemperature01() {
  return Column(
    children: [
      Text(
        "Now",
        style: TextStyle(color: Colors.white, fontSize: 15),
      ),
      Text(""),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(""),
      Text(
        "17°",
        style: TextStyle(color: Colors.white, fontSize: 20),
      )
    ],
  );
}

buildTemperature02() {
  return Column(
    children: [
      Text(
        "09",
        style: TextStyle(color: Colors.white, fontSize: 15),
      ),
      Text(""),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(""),
      Text(
        "19°",
        style: TextStyle(color: Colors.white, fontSize: 20),
      )
    ],
  );
}

buildTemperature03() {
  return Column(
    children: [
      Text(
        "10",
        style: TextStyle(color: Colors.white, fontSize: 15),
      ),
      Text(""),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(""),
      Text(
        "23°",
        style: TextStyle(color: Colors.white, fontSize: 20),
      )
    ],
  );
}

buildTemperature04() {
  return Column(
    children: [
      Text(
        "11",
        style: TextStyle(color: Colors.white, fontSize: 15),
      ),
      Text(""),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(""),
      Text(
        "26°",
        style: TextStyle(color: Colors.white, fontSize: 20),
      )
    ],
  );
}

buildTemperature05() {
  return Column(
    children: [
      Text(
        "27",
        style: TextStyle(color: Colors.white, fontSize: 15),
      ),
      Text(""),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(""),
      Text(
        "27°",
        style: TextStyle(color: Colors.white, fontSize: 20),
      )
    ],
  );
}

buildTemperature06() {
  return Column(
    children: [
      Text(
        "13",
        style: TextStyle(color: Colors.white, fontSize: 15),
      ),
      Text(""),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(""),
      Text(
        "28°",
        style: TextStyle(color: Colors.white, fontSize: 20),
      )
    ],
  );
}

buildTempDay0() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text(
        "Today",
        style: TextStyle(
            fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
      ),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(
        "12°",
        style: TextStyle(
          fontSize: 20,
          color: Colors.grey,
        ),
      ),
      Icon(
        Icons.arrow_forward,
        color: Colors.lime,
      ),
      Text(
        "31°",
        style: TextStyle(
          fontSize: 20,
          color: Colors.white,
        ),
      )
    ],
  );
}

buildTempDay1() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text(
        "Mon",
        style: TextStyle(
            fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
      ),
      Icon(
        Icons.thunderstorm,
        color: Colors.white,
      ),
      Text(
        "16°",
        style: TextStyle(
          fontSize: 20,
          color: Colors.grey,
        ),
      ),
      Icon(
        Icons.arrow_forward,
        color: Colors.orange,
      ),
      Text(
        "32°",
        style: TextStyle(
          fontSize: 20,
          color: Colors.white,
        ),
      )
    ],
  );
}

buildTempDay2() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text(
        "Tue",
        style: TextStyle(
            fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
      ),
      Icon(
        Icons.sunny,
        color: Colors.orangeAccent,
      ),
      Text(
        "19°",
        style: TextStyle(
          fontSize: 20,
          color: Colors.grey,
        ),
      ),
      Icon(
        Icons.arrow_forward,
        color: Colors.deepOrange,
      ),
      Text(
        "36°",
        style: TextStyle(
          fontSize: 20,
          color: Colors.white,
        ),
      )
    ],
  );
}

buildTempDay3() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text(
        "Wed",
        style: TextStyle(
            fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
      ),
      Icon(
        Icons.thunderstorm,
        color: Colors.white,
      ),
      Text(
        "14°",
        style: TextStyle(
          fontSize: 20,
          color: Colors.grey,
        ),
      ),
      Icon(
        Icons.arrow_forward,
        color: Colors.yellowAccent,
      ),
      Text(
        "28°",
        style: TextStyle(
          fontSize: 20,
          color: Colors.white,
        ),
      )
    ],
  );
}

buildTempDay4() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text(
        "Thu",
        style: TextStyle(
            fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
      ),
      Icon(
        Icons.cloud,
        color: Colors.white,
      ),
      Text(
        "13°",
        style: TextStyle(
          fontSize: 20,
          color: Colors.grey,
        ),
      ),
      Icon(
        Icons.arrow_forward,
        color: Colors.green,
      ),
      Text(
        "19°",
        style: TextStyle(
          fontSize: 20,
          color: Colors.white,
        ),
      )
    ],
  );
}

buildTempDay5() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text(
        "Fri",
        style: TextStyle(
            fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
      ),
      Icon(
        Icons.thunderstorm,
        color: Colors.white,
      ),
      Text(
        "13°",
        style: TextStyle(
          fontSize: 20,
          color: Colors.grey,
        ),
      ),
      Icon(
        Icons.arrow_forward,
        color: Colors.lime,
      ),
      Text(
        "26°",
        style: TextStyle(
          fontSize: 20,
          color: Colors.white,
        ),
      )
    ],
  );
}

buildTempDay6() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text(
        "Sat",
        style: TextStyle(
            fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
      ),
      Icon(
        Icons.cloud,
        color: Colors.white,
      ),
      Text(
        "17°",
        style: TextStyle(
          fontSize: 20,
          color: Colors.grey,
        ),
      ),
      Icon(
        Icons.arrow_forward,
        color: Colors.orangeAccent,
      ),
      Text(
        "28°",
        style: TextStyle(
          fontSize: 20,
          color: Colors.white,
        ),
      )
    ],
  );
}

buildTempDay7() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text(
        "Sun",
        style: TextStyle(
            fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
      ),
      Icon(
        Icons.thunderstorm,
        color: Colors.white,
      ),
      Text(
        "18°",
        style: TextStyle(
          fontSize: 20,
          color: Colors.grey,
        ),
      ),
      Icon(
        Icons.arrow_forward,
        color: Colors.orange,
      ),
      Text(
        "31°",
        style: TextStyle(
          fontSize: 20,
          color: Colors.white,
        ),
      )
    ],
  );
}
